Installation
=======================================================

Step by step
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- Import the unity package
- Edit the file Plugins/Android/AndroidManifest.xml
    - Add or replace if exist:

.. code-block:: xml

        <meta-data android:name="com.facebook.sdk.ApplicationId" android:value="fb<Facebook app Id>" />
        <provider android:name="com.facebook.FacebookContentProvider" android:authorities="com.facebook.app.FacebookContentProvider<Facebook app Id>" android:exported="true" />

- In your initialization code (in Start or Awake) call this line

.. code-block:: c#
    
        GTVSDK.Instance.Init(partner:<Partner Id>, isProduction:<true if production environment / false if development>, appsflyerKey:<appsflyer key>, appsflyerId:<appsflyer id for ios>);
    