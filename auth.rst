Authentication
=======================================================

Login and Register
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Call this method to show the authentication webview

.. code-block:: c#
    
        GTVSDK.Instance.ShowLogin(System.Action<string> loginCallback, System.Action playnowCallback, System.Action<string> fbLoginCallback)
        // loginCallback: a callback return userHash if login with ID success
        // playnowCallback: a callback that is called if the "play now" button is pressed
        // fbLoginCallback: a callback return userHash if login with facebook success
