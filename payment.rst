Payment
=======================================================

IAP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Call this method to start the iap process

.. code-block:: c#
    
        GTVSDK.Instance.StartIapProcess(GameUserData userData, System.Action<IAPResponse> onIapCompleted)
        // userData: an object contain user information
        // onIapCompleted: a callback return the status and data of IAP flow

.. code-block:: c#

        // the content of this class is depended on the partner
        public class GameUserData
        {
            public int level;
            public string platform; // ignore
            public int roleId;
            public int serverId;
            public string userHash; // ignore
            public int partner; 
            public string product_id;
        }

.. code-block:: c#
        
        public class IAPResponse
        {
            public IAPResponseCode ErrorCode { get; set; } // ErrorCode = 1: success, 11: cancel, other: failed
            public string Message { get; set; }
            public string Receipt { get; set; }
        }